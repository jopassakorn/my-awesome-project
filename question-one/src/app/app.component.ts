import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public IS_PRIME = 'isPrime';
  public IS_FIBONACCI = 'isFibonacci';
  title = 'question-one';
  userInputNumber = 1;
  calculateResult = false;
  calculateType = this.IS_PRIME;

  ngOnInit(): void {
    this.setCalculateResult();
  }

  setCalculateResult(): void {
    this.calculateResult = this.calculateType === this.IS_PRIME ?
      this.isPrimeNumber(this.userInputNumber) :
      this.isFibonacciNumber(this.userInputNumber);
  }

  convertToPositiveInteger(userInputNumber: number): number {
    if (!userInputNumber || userInputNumber < 0) {
      userInputNumber = 1;
    } else {
      userInputNumber = Math.round(userInputNumber);
    }
    return userInputNumber;
  }

  isFibonacciNumber(inputNumber: number, prevFibNum = 2, currentFibNum = 2): boolean {
    let isFibonacci;
    const nextFibNum = prevFibNum + currentFibNum;
    if (inputNumber <= 2) {
      isFibonacci = true;
    } else if (inputNumber === nextFibNum) {
      isFibonacci = true;
    } else if (inputNumber < nextFibNum) {
      isFibonacci = false;
    } else {
      isFibonacci = this.isFibonacciNumber(inputNumber, currentFibNum, nextFibNum);
    }
    return isFibonacci;
  }

  isPrimeNumber(input: number, devisor = Math.floor((input / 2))): boolean {
    let isPrime;
    if (input === 1) {
      isPrime = false;
    } else if (input === 2 || input === 3) {
      isPrime = true;
    } else if (input % devisor === 0) {
      isPrime = false;
    } else if (devisor <= 3) {
      isPrime = true;
    } else {
      isPrime = this.isPrimeNumber(input, devisor - 1);
    }
    return isPrime;
  }
}
