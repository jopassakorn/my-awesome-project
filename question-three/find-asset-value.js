const assetName = process.argv[2];

const puppeteer = require('puppeteer');
const pageUrl = 'https://codequiz.azurewebsites.net/';

readAssetValue = async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(pageUrl);
    const input = await page.$('input');
    input.click();
    await page.screenshot({path: 'example.png'});

    const tdInnerTextArray = await page
      .$$eval('tr:not(:first-child) td:first-child, tr:not(:first-child) td:nth-child(2)', 
      node => node.map(td => td.innerText));
    const searchIndex = tdInnerTextArray.indexOf(assetName);
    console.log(searchIndex != -1 ? tdInnerTextArray[searchIndex + 1] : 'Asset is not found');

    await browser.close();
}

readAssetValue();