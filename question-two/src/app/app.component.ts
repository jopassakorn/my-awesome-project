import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'question-two';
  categoryList: Array<string> = [];
  filteredList: Array<string> = [];
  filteredKeyword = '';
  setTimeOutFn: any;

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit(): void {
    this.httpClient.get<Array<string>>('https://api.publicapis.org/categories')
      .subscribe(res => {
        this.categoryList = res;
        this.filteredList = res;
      });
  }

  filteredWord(): void {
    clearTimeout(this.setTimeOutFn);
    this.setTimeOutFn = setTimeout(() => {
      const lowerFilteredWord = this.filteredKeyword.toLowerCase();
      this.filteredList = this.categoryList.filter(cat => cat.toLowerCase().includes(lowerFilteredWord));
    }, 1000);
  }
}
